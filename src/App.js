import React, { useState, useEffect } from 'react'
import FormComponent from './components/forms'
import { Router, Switch, Route } from 'react-router-dom'
import { createBrowserHistory } from "history"
import { initStages } from './components/forms/utils'
import loadable from '@loadable/component'


// eslint-disable-next-line import/no-anonymous-default-export
export default () => {

  const KycSkeleton = loadable(() => import('./components/forms/widgets/personalKycComponent/skeleton'))
  const history = createBrowserHistory()
  const [ dataForm, setDataForm ] = useState()
  
  const init = async() => {
    const _dataForm = await initStages({
      personType:'natural',
      level:'level_1',
      formName:'personal',
    })

    setDataForm(_dataForm)
  }

  useEffect(()=> { 
    init()
  }, [])


  return(
    <Router 
      history={history}
      >
        <Switch>
          <Route exact path="/">
          {
            dataForm ?
              <FormComponent
                handleDataForm={{dataForm, setDataForm}}
              />
            :
              <KycSkeleton/>
          }
          </Route>
        </Switch>
    </Router>
  )

}


