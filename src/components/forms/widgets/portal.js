import { createPortal } from 'react-dom';

const Portal = ({ children, target }) => {

  if(!target){return null}
  return createPortal(
    children,
    target
  );
}

export default Portal
