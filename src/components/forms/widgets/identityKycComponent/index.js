import React, { useEffect } from "react";
import useStage from '../../hooks/useStage'
import Layout from '../layout'
import { MainContainer } from './styles'
import './index.css'
import front from './assets/front.png'
import back from './assets/back.png'
import moment from "moment";
import "moment/locale/es";

moment.locale("es");

const IdentityKycComponent = ({ handleDataForm:{ dataForm }, ...props }) => {

    const stageManager = useStage(
        // create the form stages
        Object.keys(dataForm?.handleError?.errors || dataForm.stages),
        dataForm.stages
    )

    const {
        prevStage,
        nextStage,
        currentStage,
        stageController,
        finalStage,
        stageData,
        setStageData,
        stageStatus,
        setStageStatus
      } = stageManager

    //   11/11/1199
    console.log('||||||||||||| to timestamp ==> ', typeof new Date("11/11/1980").getTime())

      return (
        <Layout>
            <MainContainer>
                <h1 className="identity_title">Verificación avanzada</h1>

                <div className="cardContainer">
                    <div className="face front">
                        <img 
                            src={front}
                            width={290}
                            height={190}
                            alt=""
                        />
                    </div>
                    <div className="face back">
                         <img 
                            src={back}
                            width={290}
                            height={190}
                            alt=""
                        />
                    </div>
                </div>
            </MainContainer>
        </Layout>
      )

    // @id_type
    // cedula_extranjeria
    // cedula_ciudadania
    // pasaporte
    // pep

    // return(
    //     <div> 
    //         <h1>Identity kyc component</h1>
    //         <h3 onClick={nextStage} >Siguiente</h3>
    //         <h3 onClick={prevStage} >Anterior</h3>
    //         <h4>Estas en : {stageController[currentStage]}</h4>
    //         <p>currentStage: {currentStage}</p>
    //         {
    //             finalStage &&
    //             <h2>Has finalizado el formulario</h2>
    //         }
    //     </div>
    // )
}

export default IdentityKycComponent