import styled from "styled-components"

export const MainContainer = styled.div`
    text-align: center;
    display:grid;
    grid-template-rows: auto 1fr;
    perspective: 1000px;
    justify-items:center;

   .identity_title{
       margin-top:50px;
       margin-bottom:80px;
       text-align:center;
   }
`
