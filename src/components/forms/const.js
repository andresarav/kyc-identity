export const PRIMARY_COLOR = '#00ffc4'

export const INFO_URL_API = 'https://info.bitsenda.com'

export const LABEL_COLOR = {
    default:'gray',
    error:'red'
  }
